$(document).ready(function() {
    if ($(window).width() > 1024 && $('.main-animated').length) {
        var controller = new ScrollMagic.Controller();

        //course block fade
        var fadeBlock = new TimelineMax()
            .to(".course-module--main", 1, {"opacity": 0, "y": "-80", ease: Linear.easeNone});

        new ScrollMagic.Scene({
            triggerElement: ".course-module--main",
            triggerHook: "onLeave",
            duration: "50%"
        })
        .setTween(fadeBlock)
        .setPin(".course-module--main")
        .addTo(controller);

        //letters translate & fade
        var letterC = new TimelineMax()
            .to(".animated-letter._c", 1, {"top": "95px", ease: Linear.easeNone});

        new ScrollMagic.Scene({
            triggerElement: ".course-module--main",
            triggerHook: "onLeave",
            duration: "50%"
        })
            .on("end", hideLetters)
            .on("progress", showLetters)
            .setTween(letterC)
            .addTo(controller);

        function hideLetters() {
            new TimelineMax()
                .to(".animated-letter._c", 0.3, {"opacity": 0, ease: Linear.easeNone}, "letters")
                .to(".animated-letter._i", 0.3, {"opacity": 0, ease: Linear.easeNone}, "letters");
        }

        function showLetters() {
            new TimelineMax()
                .to(".animated-letter._c", 0.3, {"opacity": 1, ease: Linear.easeNone}, "letters")
                .to(".animated-letter._i", 0.3, {"opacity": 1, ease: Linear.easeNone}, "letters");
        }
    }
});