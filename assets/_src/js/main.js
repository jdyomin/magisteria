$(function() {

    var $win = $(window);

    if ($('.dots').length) {
        $('.dots').dotdotdot({
          // Options go here
        });
    };

    if ($('.js-section-menu-control').length) {
        $('.js-section-menu-control').on('click', function() {
            $(this).find('.section-nav-sublist').toggleClass('show');
        })
    };

    if ($('.sound-button').length) {
        $('.sound-button').on('click', function() {
            $(this).toggleClass('_sound-on');
        })
    };

    if ($('.js-section-nav-trigger').length) {
        $('.js-section-nav-trigger').on('click', function() {
            $(this).closest('.js-section-nav').toggleClass('expanded');
        });
    }

    $('.js-section-menu-control').on('click', function() {
        $(this).toggleClass('expanded');
    })

    if ($('.js-sticky').length) {
        var elements = $('.js-sticky');
        Stickyfill.add(elements);
    };

    if ($('.extras-list').length) {
        $.each($('.extras-list'), function() {

            var items = $(this).find('li');

            $.each(items, function() {
                if ($(this).closest('.lecture-info').length) {
                    var innerCounter = $(this).index() + 2;
                } else {
                    var innerCounter = $(this).index() + 1;
                }
                var innerCounterEl = '<span class="inner-counter"></span>';
                $(this).find('a').prepend(innerCounterEl);
                $(this).find('.inner-counter').text(innerCounter);
            })

            if ($('.js-lecture-full').length) {
                var counter = $(this).closest('.js-lecture-full').index() + 1 + '.';
            } else {
                var counter = $(this).attr('data-number') + '.';
            }
            var counterEl = '<span class="counter"></span>';
            $(this).find('li a').prepend(counterEl);
            $(this).find('li .counter').text(counter);
        });

        $('.extras-list__play-btn').on('click', function() {
            $(this).toggleClass('playing');
        })
    }

    if ($('.js-play').length) {
        var adjustBtns = $('.js-adjust, .js-play')
        adjustBtns.on('click', function() {
            var scrollTarget = $('.js-player').outerHeight() - $(window).height();
            //console.log(scrollTarget);
            $('html, body').animate({
                scrollTop: scrollTarget
            }, 600);
        })
    }

    if ($('._player').length) {
        $('.play-btn-big').on('click', function() {
            $(this).toggleClass('paused');
            $('body').toggleClass('playing');
        });

        if ($(window).width() < 600) {
            $('.js-gallery-trigger').on('click', function() {
                var bottom = $('.js-gallery-slider-wrapper').outerHeight() + 10;
                if ($('.js-gallery-slider-wrapper').hasClass('show')) {
                    $('.js-gallery-controls').css('bottom', '10px');
                } else {
                    $('.js-gallery-controls').css('bottom', bottom);
                }
            })
        }
    }

    if ($('.js-gallery-slider').length) {
        var gallerySlider = new Swiper('.js-gallery-slider', {
            navigation: {
                prevEl: '.js-gallery-controls .swiper-button-prev',
                nextEl: '.js-gallery-controls .swiper-button-next'
            },
            scrollbar: {
                el: '.swiper-scrollbar',
                draggable: true,
            },
            slidesPerView: 'auto',
            on: {
                resize: function() {
                    gallerySlider.update();
                }
            },
            observer: true,
        });
    }
    

    $('.js-login').on('click', function() {
        $('.js-popup._registration').addClass('opened');
        $('body').addClass('overflow');
    })

    $('.js-language-toggle').on('click', function() {
        $('.js-language').toggleClass('opened');
    });

    $('.js-language-picker li').on('click', function(e) {
        e.preventDefault();
        $('.js-language-picker li').removeClass('selected');
        $(this).addClass('selected');
        var lang = $(this).find('a').attr('data-lang');
        $('.js-language-toggle span').text(lang);
    });

    $('.js-search-trigger').on('click', function() {
        $('.js-search').addClass('opened');
    });

    $('.js-search-close').on('click', function() {
        $('.js-search').removeClass('opened');
        $('.js-search input').val('');
    });

    $('.js-filter-toggle').on('click', function(e) {
        e.preventDefault();

        $(this).toggleClass('active');
        $('.js-filters').toggleClass('opened');
    });

    $('.js-menu-trigger').on('click', function() {
        $('.js-header').toggleClass('opened');
        $('.js-header').toggleClass('_fixed');
        $('body').toggleClass('overflow');
        $('main').toggleClass('invisible');
    });

    $('.js-search-field').on('focus', function() {
        $('.js-search').addClass('opened');
    });

    $('.js-tooltip-trigger').on('click', function() {
        $('.js-user-block').toggleClass('opened');
    });

    $('.js-filters-mobile-trigger').on('click', function() {
        $('.js-filters-mobile').toggleClass('opened');
    });

    $('.js-lectures-list-trigger').on('click', function() {
        $(this).closest('.js-lectures-menu').toggleClass('opened');
        $('body').toggleClass('overflow');
        $('#fp-nav').toggleClass('hide');
        if ($(this).closest('.js-lectures-menu').hasClass('opened')) {
            $('.js-filters').removeClass('opened');
        }
    });

    $('.js-menu-tab-control').on('click', function() {
        $(this).toggleClass('expanded');
        $(this).find('.menu-nav-sublist').css('top', '44px');
    });

    if ($(window).width() > 1279) {
        $('.js-lectures-menu-nav').css('width', $('.lectures-menu-nav__list').width());
    };

    $('.js-menu-nav-trigger').on('click', function() {
        $('.lectures-menu-nav__list').toggleClass('show');
        $('.lectures-menu-nav__list').css('height', $(window).height() - $('.js-header').height());
    })

    $('[data-fancybox]').fancybox();

    $('.js-scroll-link').on("click", function (e) {
        var $target = $($(this).attr('href')),
            targetOffset = $target.offset().top;

        if ($target.length) {
            e.preventDefault();

            // trigger scroll
            $('html, body').animate({
                scrollTop: targetOffset
            }, 600);
        }
    });

    $('.js-tab-control').on('click', function() {
        var tabs = $(this).closest('.js-tabs').find('.js-tab');
        var n = $(this).index();
        $(this).siblings().removeClass('active');
        $(this).addClass('active');
        $.each(tabs, function() {
            $(this).removeClass('active')
            if ($(this).index() === n) {
                $(this).addClass('active');
            }
        })
    });

    $('.course-module__read-more').on('click', function() {
        $(this).prev('.course-module__course-descr').toggleClass('full');
        $(this).toggleClass('opened');
        return false;
    });

    if ($('.js-author-block-trigger').length) {

        $('.js-author-block-trigger').on('click', function() {
            $(this).toggleClass('active');
            $(this).next('.js-author-block-content').toggleClass('opened');
        })
    };

    var padding = $('.js-header').height();
    $('main').css('padding-top', padding);

    var lastScrollTop = 0;

    $('.js-gallery-trigger').on('click', function() {
        if ($(this).closest('.js-gallery-controls').hasClass('hide')) {
            if ($(window).scrollTop() >= ($('#recommend').offset().top - $(window).height())) {
                $('html, body').animate({
                    scrollTop: $('#recommend').offset().top - $(window).height()
                }, 600, function () {
                    openGallerySlider();
                });
            } else {
                openGallerySlider();
            }
        } else {
            closeGallerySlider();
        }
    });

    function openGallerySlider() {
        var controls = $('.js-gallery-controls'),
            wrap = $('.js-gallery-slider-wrapper'),
            stickyBlock = $('.js-sticky-block');

        controls.removeClass('hide').addClass('show');
        wrap.addClass('show');
        stickyBlock.addClass('slider-opened');
    }

    function closeGallerySlider() {
        var controls = $('.js-gallery-controls'),
            wrap = $('.js-gallery-slider-wrapper'),
            stickyBlock = $('.js-sticky-block');

        controls.addClass('hide').removeClass('show');
        wrap.removeClass('show');
        stickyBlock.removeClass('slider-opened');
    }

    $(window).scroll(function(event) {
        var controls = $('.js-gallery-controls');
        var windowHeight = $(window).height();

        if ($('.js-sticky-block').length) {
            var coordTop = $('#recommend').offset().top;

            if (($(window).scrollTop() + windowHeight / 2 + 120) >= coordTop) {
                $('.js-sticky-block').css('position', 'absolute').css('top', coordTop).css('margin-top', '-100px');
            } else {
                $('.js-sticky-block').css('position', 'fixed').css('top', '50%').css('margin-top', '0');
            }

            if ($(window).width() < 768 ) {
                if (($(window).scrollTop() + windowHeight) >= coordTop) {
                    $('.js-sticky-block').css('position', 'absolute').css('top', coordTop).css('bottom', 'auto').css('margin-top', '-100px');
                } else {
                    $('.js-sticky-block').css('position', 'fixed').css('top', 'auto').css('bottom', '30px').css('margin-top', '0');
                }
            }
        }

        if (controls.length) {
            var coordTop = $('#recommend').offset().top;

            if ($(window).width() < 768 ) {
                if ($(window).scrollTop() + windowHeight >= coordTop) {
                    controls.css('position', 'absolute').css('bottom', 'auto').css('top', coordTop - 55);
                    if (controls.hasClass('show')) {
                        closeGallerySlider();
                    }
                } else {
                    $('.js-gallery-controls').css('position', 'fixed').css('top', 'auto').css('bottom', '10px').css('margin-top', '0');
                }
            } else {
                if ($(window).scrollTop() + windowHeight >= coordTop) {
                    controls.css('position', 'absolute').css('top', coordTop).css('transform', 'none').css('bottom', 'auto');
                    if (controls.hasClass('show')) {
                        closeGallerySlider();
                    }
                } else {
                    $('.js-gallery-controls').css('position', 'fixed').css('bottom', '20px').css('top', 'auto').css('margin-top', '-60px');
                }
            }

            if ($('.js-player').length) {
                if ($(window).scrollTop() < $('.js-player').outerHeight()) {
                    controls.removeClass('visible');
                    controls.fadeOut();
                } else {
                    controls.addClass('visible');
                    controls.fadeIn();
                }
            }
        }

        var st = $(this).scrollTop();
        var padding = $('.js-header').height();

        if ($(this).scrollTop() === 0) {
            $('.js-header').removeClass('_animate');
            $('main').css('padding-top', '0px');
        };

        if ((st > padding) && (st>lastScrollTop)) {
            $('.js-header').addClass('_animate');
            if (($(window).width() > 639) && ($(window).width() < 900)) {
                if ($('.author-block__image').length) {
                    var imageCoordTop = $('.author-block__image').offset().top;
                    var imageCoordBottom = $('.author-block:nth-child(2').offset().top;

                    if ($(window).scrollTop() >= imageCoordTop) {
                        $('.author-block__image').css('position', 'sticky').css('top', $(window).scrollTop());
                    }

                    if ($(window).scrollTop() > imageCoordBottom) {
                        $('.author-block__image').css('position', 'static').css('top', '0');
                    }
                };
            };
            if ($(window).width() > 899) {
                if ($('.project-info__image-block').length) {
                    var imageCoordTop = $('.project-info__image-block').offset().top;
                    var imageCoordBottom = $('.project-info__image-block').offset().bottom;

                    if ($(window).scrollTop() >= imageCoordTop) {
                        $('.project-info__image-block').css('position', 'sticky').css('top', $(window).scrollTop());
                    }

                    if ($(window).scrollTop() > imageCoordBottom) {
                        $('.project-info__image-block').css('position', 'static').css('top', '0');
                    }
                };
            }
        } else {
            $('.js-header').removeClass('_animate');
            if (($(window).width() > 639) && ($(window).width() < 900)) {
                if ($('.author-block__image').length) {
                    var imageCoordTop = $('.author-block__image').offset().top;
                    var imageCoordBottom = $('.author-block:nth-child(2').offset().top;

                    if ($(window).scrollTop() <= imageCoordTop) {
                        $('.author-block__image').css('position', 'sticky').css('top', $(window).scrollTop());
                    }

                    if ($(window).scrollTop() < imageCoordBottom) {
                        $('.author-block__image').css('position', 'static').css('top', '0');
                    }
                };
            };
            if ($(window).width() > 899) {
                if ($('.project-info__image-block').length) {
                    var imageCoordTop = $('.project-info__image-block').offset().top;
                    var imageCoordBottom = $('.project-info__image-block').offset().bottom;

                    if ($(window).scrollTop() <= imageCoordTop) {
                        $('.project-info__image-block').css('position', 'sticky').css('top', $(window).scrollTop());
                    }

                    if ($(window).scrollTop() < imageCoordBottom) {
                        $('.project-info__image-block').css('position', 'static').css('top', '0');
                    }
                }
            }
        };

        if (st < lastScrollTop){
            $('main').css('padding-top', padding);
        };
        lastScrollTop=st;

        if ($('.js-social-start').length) {

            var socialStart = $('.js-social-start');

            if (st < socialStart.offset().top + 147) {
                $('.js-social').removeClass('_fixed');
                $('.js-social').css('top', '0').css('bottom', 'auto');
            }

            if (st > socialStart.offset().top + 147) {
                $('.js-social').addClass('_fixed');
                $('.js-social').css('bottom', 'auto').css('top', '0');
            }

            if (st > (socialStart.offset().top + socialStart.outerHeight() - $('.js-social').outerHeight())) {
                $('.js-social').removeClass('_fixed');
                $('.js-social').css('top', 'auto').css('bottom', '0');
            }

            if (st < socialStart.offset().top - 63) {
                $('.js-play').removeClass('_fixed');
                $('.js-play').css('bottom', 'auto').css('top', '10px');
            }

            if (st > socialStart.offset().top - 63) {
                $('.js-play').addClass('_fixed');
                $('.js-play').css('bottom', 'auto').css('top', '10px');
            }

            if (st > (socialStart.offset().top + socialStart.outerHeight() - $('.js-play').outerHeight() - 98)) {
                $('.js-play').removeClass('_fixed');
                $('.js-play').css('bottom', '0').css('top', 'auto');
            }
        };

        if ($('.js-player').length) {
            if (st > ($('.js-player').outerHeight() - 53)) {
                $('.js-lectures-menu').removeClass('_dark');
                $('.js-lectures-menu').addClass('_fixed');
            } else {
                $('.js-lectures-menu').addClass('_dark');
                $('.js-lectures-menu').removeClass('_fixed');
            }

            if (st < $('.js-player').outerHeight()) {
                closeGallerySlider();
            }
        }
    });

    var tooltips = $('.js-language, .js-user-block, .js-speed, .js-contents, .js-share');

    $(document).mouseup(function (e) {
        if (tooltips.has(e.target).length === 0){
            tooltips.removeClass('opened');
        }
    });

    var authorsCounter = function() {
        if ($('.author-card').length) {
            var authors = $('.author-card').length;
        };
        if (($(window).width() > 1024) && ((authors + 2) % 4 === 0)) {
            $('.dummy').css('display', 'none');
            return;
        } else {
            $('.dummy').css('display', 'block');
        };
        if (($(window).width() < 1025) && ((authors + 2) % 3 === 0)) {
            $('.dummy').css('display', 'none');
            return;
        } else {
            $('.dummy').css('display', 'block');
        };
        if (($(window).width() < 900) && (authors % 2 === 0)) {
            $('.dummy').css('display', 'none');
            return;
        } else {
            $('.dummy').css('display', 'block');
        };
        if ($(window).width() < 640) {
            $('.dummy').css('display', 'none');
            return;
        } else {
            $('.dummy').css('display', 'block');
        };
    };

    authorsCounter();
    $(window).on('resize', function() {
        authorsCounter();
    });

    $('.js-social-trigger').on('click', function() {
        $('.player-wrapper.active').find('.js-share').addClass('opened');
        $('.js-lectures-menu').addClass('hide');
        $('#fp-nav').addClass('hide');
        if ($(window).width() < 901) {
            $('body').addClass('overflow');
        }
    });

    $('.js-popup-close').on('click', function() {
        $(this).closest('.js-popup').removeClass('opened');
        $('body').removeClass('overflow');
        $('.js-lectures-menu').removeClass('hide');
        $('#fp-nav').removeClass('hide');
    });

    if (!($('body').hasClass('fade'))) {
        $('.js-speed-trigger').on('click', function() {
            $('.js-contents-trigger').removeClass('active');
            $(this).toggleClass('active');
            $(this).closest('.player-wrapper').find('.js-speed').toggleClass('opened');
            $(this).closest('.player-wrapper').find('.js-contents').removeClass('opened');
        });

        $('.js-contents-trigger').on('click', function() {
            $('.js-speed-trigger').removeClass('active');
            $(this).toggleClass('active');
            $(this).closest('.player-wrapper').find('.js-contents').toggleClass('opened');
            $(this).closest('.player-wrapper').find('.js-speed').removeClass('opened');
        });

        $('.js-fullscreen').on('click', function() {
            $(this).toggleClass('active');
        })
    }

    $(document).ready(function () {
        $(this).keydown(function(eventObject) {
            if (eventObject.which == 27) {
                tooltips.removeClass('opened');
            }
        });
    });

    $('.player-frame').on('click', function() {
        if ($(window).width() < 900) {
            $(this).find('.player-block__controls').addClass('show');
        }
    });

    var maskedImages = $('image');
    if (maskedImages.length) {
        $.each(maskedImages, function() {
            var imageWidth = $(this).closest('svg').attr('width');
            var imageheight = $(this).closest('svg').attr('height');
            $(this).attr('width', parseInt(imageWidth) + 150);
            $(this).attr('height', imageheight);
        })
    };

    var imageSize = function() {
        var lectureImg = $('.lecture-announce__image-block img');
        var imgSizeV = $('.lecture-announce__info-block').outerHeight();
        var imgSizeH = $('.lecture-announce__info-block').outerWidth();

        var imgAspectRatio = $(this).width() / $(this).height();
        var txtAspectRatio = imgSizeH / imgSizeV;

        var deltaH = lectureImg.width() - imgSizeH;
        var deltaV = lectureImg.height() - imgSizeV;

        if ($(window).width() > 639) {
            if ((deltaH < deltaV) && (imgAspectRatio > txtAspectRatio)) {
                lectureImg.css('width', imgSizeH).css('height', 'auto');
            } else {
                lectureImg.css('width', 'auto').css('height', imgSizeV);
            }
        } else {
            if ((deltaH < deltaV) && (imgAspectRatio < txtAspectRatio)) {
                lectureImg.css('width', imgSizeH).css('height', 'auto');
            } else {
                lectureImg.css('width', 'auto').css('height', imgSizeV);
            }
        }
    };

    imageSize();
    $(window).on('resize', function() {
        imageSize();
    });

    if ($('.gallery').length && $(window).width() < 1280) {
        var galleryImageSize = function() {
            var image = $('.gallery-item__image img');
            var imgSizeV = $('.gallery-item').outerHeight();
            var imgSizeH = $('.gallery-item').outerWidth();

            var imgAspectRatio = $(this).width() / $(this).height();
            var txtAspectRatio = imgSizeH / imgSizeV;

            $.each(image, function() {
                var deltaH = imgSizeH - $(this).width();
                var deltaV = imgSizeV - $(this).height();
                if (deltaH < deltaV){
                    $(this).css('width', 'auto').css('height', '100%');
                } else {
                    $(this).css('width', '100%').css('height', 'auto');
                }
            })
        }

        galleryImageSize();
        $(window).on('resize', function() {
            galleryImageSize();
        });
    }

    $('.js-screen-2-link').on('click', function() {
        $(this).closest('.js-screen-1').removeClass('current');
        $('.js-screen-2').addClass('current');
    });

    if ($('._show-password').length) {
        var field = $('._show-password').find('input[type="password"]');
        field.replaceWith(field.clone().attr('type',(field.attr('type') == 'password') ? 'text' : 'password'));
    }

    $('.js-eye').on('click', function() {
        var parentEl = $(this).closest('.js-field-wrapper');
        parentEl.toggleClass('_show-password');
        if (parentEl.hasClass('_show-password')) {
            var field = parentEl.find('input[type="password"]');
            field.replaceWith(field.clone().attr('type',(field.attr('type') == 'password') ? 'text' : 'password'))

        } else {
            var field = parentEl.find('input[type="text"]');
            field.replaceWith(field.clone().attr('type',(field.attr('type') == 'text') ? 'password' : 'text'))
        }
    });

    $('.js-eye-single').on('click', function() {
        var parentEl = $(this).closest('.js-field-wrapper');
        var nextParentEl = $(this).closest('.js-field-wrapper').next('.js-field-wrapper');
        parentEl.toggleClass('_show-password');
        nextParentEl.toggleClass('_show-password');
        if (parentEl.hasClass('_show-password')) {
            var field = parentEl.find('input[type="password"]');
            var field2 = nextParentEl.find('input[type="password"]');
            field.replaceWith(field.clone().attr('type',(field.attr('type') == 'password') ? 'text' : 'password'))
            field2.replaceWith(field2.clone().attr('type',(field2.attr('type') == 'password') ? 'text' : 'password'))

        } else {
            var field = parentEl.find('input[type="text"]');
            var field2 = nextParentEl.find('input[type="text"]');
            field.replaceWith(field.clone().attr('type',(field.attr('type') == 'text') ? 'password' : 'text'))
            field2.replaceWith(field2.clone().attr('type',(field2.attr('type') == 'text') ? 'password' : 'text'))
        }
    });

    var passFields = $('#new-password, #repeat-password');
    passFields.on('focusout', function() {
        if ($('#new-password').val() === $('#repeat-password').val()) {
            passFields.closest('.js-field-wrapper').removeClass('_invalid');
            passFields.closest('.js-field-wrapper').addClass('_valid');
            $('.js-error-message').removeClass('show');
        } else {
            passFields.closest('.js-field-wrapper').removeClass('_valid');
            passFields.closest('.js-field-wrapper').addClass('_invalid');
            $('.js-error-message').addClass('show');
        }
    });

    $('input').on('focus', function() {
        $(this).closest('div').find('label').css('opacity', '1');
    });
    $('input').on('blur', function() {
        if ($(this).val()) {
            return false;
        } else {
            $(this).closest('div').find('label').css('opacity', '0');
        }
    });

    if ($('.sublist-num').length) {
        $.each($('.sublist-num'), function() {
            var sublistNum = parseInt($(this).closest('.lectures-list__item').index() + 1) + '.' + parseInt($(this).closest('.lectures-sublist__item').index() + 1);
            $(this).text(sublistNum);
        });
    };

    $(".scrollable").mCustomScrollbar();
    var period,
        subscribeBtn = $('.js-subscribe'),
        billingStep = $('.js-billing-step'),
        billingBack = $('.js-billing-back');
    if (subscribeBtn.length) {
        subscribeBtn.on('click', function() {
            period = $(this).closest('li').find('.js-duration').text();
            $('.js-subcribe-period').text(period);
            billingStep.removeClass('active');
            billingStep.eq(1).addClass('active');
        });
        billingBack.on('click', function() {
            billingStep.removeClass('active');
            billingStep.eq(0).addClass('active');
            $('.js-subcribe-period').text('');
        })
    }

    // setTimeout(function() {
    //     $('body').addClass('fade');
    // }, 3000);

    // $(document).on('mousemove', function() {
    //     setTimeout(function() {
    //         $('body').removeClass('fade');
    //     }, 300);
    //     setTimeout(function() {
    //         $('body').addClass('fade');
    //     }, 7000);
    // });
});

var circleR = function() {
    if ($('.player-frame__play-screen-wrapper').length) {
        var rad = ($('.player-frame__play-screen-wrapper').width() / 2) - 2;
    } else {
        var rad = ($('.play-block__loader').width() / 2) - 2;
    }
    $('.svg-loader circle').attr('r', rad);
    $('.svg-loader circle').css('r', rad);
}

var circleR1 = function() {
    if ($('.lecture__play-block').length) {
        $.each($('.lecture__play-block'), function() {
            var rad = ($(this).width() / 2) - 2;
            $(this).find('.svg-loader circle').attr('r', rad);
            $(this).find('.svg-loader circle').css('r', rad);
        })

    } else {
        var rad = ($('.play-block__loader').width() / 2) - 2;
        $(this).find('.svg-loader circle').attr('r', rad);
            $(this).find('.svg-loader circle').css('r', rad);
    }

}

$(window).on('resize', function() {
    circleR();
    circleR1();
});

$(document).ready(function(val) {
    circleR();
    circleR1();
    function randomInteger(min, max) {
        var rand = min + Math.random() * (max + 1 - min);
        rand = Math.floor(rand);
        return rand;
    };

    var circle = $('.svg-loader .bar');
    var r = circle.height() / 2;
    var c = Math.PI * (r * 2);
    var pct = ((100 - val) / 100) * c;

    circle.css({strokeDasharray: c});

    // if (isNaN(val)) {
    //     val = 100;
    // }
    // else {
    //     var r = circle.height() / 2;
    //     var c = Math.PI * (r * 2);

    //     if (val < 0) { val = 0;}
    //     if (val > 100) { val = 100;}

    //     var pct = ((100 - val) / 100) * c;

    //     circle.css({ strokeDashoffset: pct});
    // }

    $.each(circle, function() {
        var val = randomInteger(0, 100);
        var pct = ((100 - val) / 100) * c;

        $(this).css({ strokeDashoffset: pct});
    })

    //circle.css({ strokeDashoffset: pct});
});